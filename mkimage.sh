#!/bin/ash

set -eu -o pipefail

if [ "$(id -u)" != 0 ]; then
        echo "Script must run with root permissions!"
        exit 1
fi

readonly SCRIPT="${0##*/}"
readonly DATE="$(date +%Y%m%d)"
readonly TMPDIR="$(mktemp -dt "${SCRIPT%.*}".XXXXXX)"
readonly LOOPDEV="$(losetup --find)"
readonly EFIDEV="${LOOPDEV}p1"
readonly BOOTDEV="${LOOPDEV}p2"
readonly ROOTDEV="${LOOPDEV}p3"
readonly APK_KEY="alpine-devel@lists.alpinelinux.org-60ac2099.rsa.pub"

: "${DISK:=out/alpine-edge-$DATE-riscv64.img}"
: "${DISK_SIZE:=2G}"
: "${DEBUG:=FALSE}"
: "${MIRROR:=http://dl-cdn.alpinelinux.org/alpine}"


cleanup() {
	[ "$DEBUG" == TRUE ] && return 0
	einfo "Cleaning up!"
	set +e
	rm -f "$TMPDIR"/var/cache/apk/*
	mountpoint -q "$TMPDIR"/boot && umount "$TMPDIR"/boot
	mountpoint -q "$TMPDIR" && umount "$TMPDIR"
	losetup --detach-all
	if [ -f "$DISK" ]; then
		einfo "Compress and create checksum"
		pigz "$DISK"
		sha512sum "$DISK".gz > "${DISK}".gz.sha512sum
	fi
}

rc_add() {
	local runlevel="$1"; shift  # runlevel name
	local services="$*"  # names of services

	local svc; for svc in $services; do
		mkdir -p "$TMPDIR"/etc/runlevels/"$runlevel"
		ln -s /etc/init.d/"$svc" "$TMPDIR"/etc/runlevels/"$runlevel"/"$svc"
		einfo " * service $svc added to runlevel $runlevel"
	done
}

einfo() {
	printf '\n\033[1;36m> %s\033[0m\n' "$@" >&2  # bold cyan
}

create_pdev() {
	local minor=0
	for dev; do
		if [ ! -b "$dev" ]; then
			einfo "Creating partition device $dev"
			mknod "$dev" b 259 "$minor"
		fi
		minor=$((minor + 1))
	done
}

trap cleanup EXIT INT

einfo "Creating disk image"
mkdir -p "${DISK%%/*}"
rm -f "${DISK%%/*}"/*
truncate -s "$DISK_SIZE" "$DISK"

einfo "Paritioning disk"
sgdisk --clear --set-alignment=1 \
 	-n 1:2048:+128MiB: -c 1:EFI \
 	-n 2:0:+256MiB: -c 2:BOOT \
	-n 3:0:: -c 3:ROOT \
	"${DISK}"

einfo "Creating dirs and mounting"
losetup -P "$LOOPDEV" "$DISK"

# Running this script in a container will not automagically generate partition
# devices, instead try to create them manually.
create_pdev "$EFIDEV" "$BOOTDEV" "$ROOTDEV"

einfo "Creating and mounting filesystems"
mkfs.vfat -F 32 -n EFI "$EFIDEV"
mkfs.ext4 -qL BOOT "$BOOTDEV"
mkfs.ext4 -qL ROOT "$ROOTDEV"
mount -t ext4 "$ROOTDEV" "$TMPDIR"
mkdir -p "$TMPDIR"/boot
mkdir -p "$TMPDIR"/etc/apk/keys
mount -t ext4 "$BOOTDEV" "$TMPDIR"/boot

einfo "Adding apk key"
wget -qP "$TMPDIR"/etc/apk/keys https://alpinelinux.org/keys/"$APK_KEY"

einfo "Adding repositories"
printf "$MIRROR/edge/%s\n" main community testing > \
	"$TMPDIR"/etc/apk/repositories

einfo "installing base packages"
apk --quiet --root "$TMPDIR" --arch riscv64 --initdb add \
	alpine-base \
	linux-firmware-none \
	linux-sophgo \
	doas \
	openssh-server \
	linux-firmware-radeon \
	linux-firmware-rtl_nic

einfo "Setup needed services"
rc_add sysinit devfs dmesg mdev hwdrivers cgroups
rc_add boot modules hwclock swap hostname sysctl bootmisc syslog
rc_add shutdown killprocs savecache mount-ro
rc_add default networking ntpd sshd

einfo "Enable getty on serial console"
sed -i '/ttyS0/s/^#//g' "$TMPDIR"/etc/inittab

einfo "Enable doas for wheel group"
echo "permit nopass :wheel" > "$TMPDIR"/etc/doas.d/wheel.conf

einfo "Creating extlinux bootloader config"
UUID=$(blkid $ROOTDEV | cut -d'"' -f4)
mkdir -p "$TMPDIR"/boot/extlinux
cat > "$TMPDIR"/boot/extlinux/extlinux.conf <<- EOF
	TIMEOUT 50
	PROMPT 1
	DEFAULT sophgo

	MENU TITLE riscv64

	LABEL sophgo
	MENU LABEL alpine linux-sophgo kernel
	KERNEL /vmlinuz-sophgo
	INITRD /initramfs-sophgo
	APPEND console=ttyS0,115200 root=UUID=$UUID rw earlycon modules=sd-mod,usb-storage,ext4 nvme_core.io_timeout=600 nvme_core.admin_timeout=600 cma=512M swiotlb=65536
	FDT /dtbs-sophgo/sophgo/mango-milkv-pioneer.dtb
	EOF

